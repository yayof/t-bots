<?php
use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('Start conversation', BotManController::class.'@startConversation');

$botman->hears('test', function ($bot) {
    $bot->reply('yayo?');
});

$botman->hears('links', BotManController::class.'@links');


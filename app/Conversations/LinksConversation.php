<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class LinksConversation extends Conversation
{

    public function askReason()
    {
        $question = Question::create("Qué link necesitas?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Games list')->value('gamesList'),
                Button::create('Games Played')->value('gamesPlayed'),
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'gamesList') {
                    $this->say("https://drive.google.com/open?id=1A4-hFlKm8BXRoKHrizZbwTiZ84VA1u6d");
                } else {
                    $this->say("https://nemestats.com/GamingGroup/Details/16426?Iso8601FromDate=2000-01-01&Iso8601ToDate=2018-12-14&tab=plays");
                }
            }
        });
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askReason();
    }
}
